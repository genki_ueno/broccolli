class CardsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :edit, :update]

  def index
    @q = Card.all.ransack(params[:q])
    @cards = @q.result(distinct: true)
  end

  def create
  end

  def show
    @card = Card.find(params[:id])
    @room = Room.new
  end

  def edit
    @card = Card.find(params[:id])
  end

  def update
    @card = Card.find(params[:id])
    if @card.update_attributes(cards_params)
      flash[:success] = "更新しました"
      redirect_to @card
    else
      render 'edit'
    end
  end

  private

  def cards_params
    params.require(:card).permit(:title, :image, :description, :price, :user_id)
  end
end
