class HomeController < ApplicationController
  before_action :authenticate_user!, except: [:index, :flash_close, :contact, :about, :portfolio]

  def index
  end

  def flash_close
  end

  def contact
  end

  def about
  end

  def fs_agreement
    if current_user && current_user.sitter_status == 1
      redirect_to welcome_path
    elsif current_user && current_user.sitter_status == 2
      redirect_to root_path
    else

    end
  end

  def agreement_check
    if params[:agreement] == '0'
      render 'fs_agreement'
    else
      current_user.update_attributes(sitter_status: 1) if current_user.sitter_status == 0
      redirect_to welcome_path
    end
  end

  def fs_welcome
    if current_user && current_user.sitter_status == '1'

    elsif current_user && current_user.sitter_status == '2'
      redirect_to root_path
    end
  end

  def first_create
    @card = Card.new(welcome_params)
    if @card.save
      current_user.update_attributes(sitter_status: 2) if current_user.sitter_status == 1
      redirect_to "/flower_sitter/congrats/#{@card.id}"
    else
      render 'fs_welcome'
    end
  end

  def fs_congrats
    @card = Card.find(params[:id])
  end

  def portfolio

  end

  private

  def welcome_params
    params.require(:card).permit(:title, :description, :price, :user_id, :image)
  end
end
