class MessagesController < ApplicationController
  before_action :authenticate_user!

  def create
    message = Message.new(message_params)
    if message.save
      redirect_to room_path message.room_id
    else
      redirect_to root_path
    end
  end

  private

  def message_params
    params.require(:message).permit(:content, :room_id, :user_id)
  end
end
