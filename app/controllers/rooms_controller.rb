class RoomsController < ApplicationController
  before_action :authenticate_user!

  def index
    @rooms = Room.where("(guest_id = ?) OR (sitter_id = ?)", current_user.id, current_user.id)
    # @rooms_for_sitter = Room.where(sitter_id: current_user.id)
  end

  def create
    room = Room.find_or_initialize_by(sitter_id_params)
    if room.save
      redirect_to room
    else
      redirect_to root_path
    end
  end

  def show
    @room = Room.find(params[:id])
    @room.messages.where.not("(has_read = ?) OR (user_id = ?)", true, current_user.id).each do |msg|
      msg.update_attributes(has_read: true)
    end
  end

  private

  def sitter_id_params
    params.require(:room).permit(:guest_id, :sitter_id)
  end
end
