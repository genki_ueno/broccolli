module ApplicationHelper

  def full_title(page_title = '')
    base_title = "Broccolli"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def meta_description(description = '')
    base_content = "自宅の植物を近くのお花屋さんや詳しい人(フラワーシッター)に預かってもらうための日本初マッチングサービス。新しくフラワーシッターとしての登録も１分で可能！"
    if description.empty?
      base_content
    else
      description
    end
  end

  def meta_keywords(keywords = '')
    keywords
  end
end
