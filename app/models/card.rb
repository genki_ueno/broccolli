class Card < ApplicationRecord
  belongs_to :user
  has_one_attached :image
  validates :user_id, :title, :description, :price, :image, presence: true

  # def self.ransackable_attributes(auth_object = nil)
  #   %w[title description]
  # end
  #
  # def self.ransackable_associations(auth_object = nil)
  #
  # end
end
