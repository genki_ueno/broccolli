class Room < ApplicationRecord
  belongs_to :guest,  class_name: 'User'
  belongs_to :sitter, class_name: 'User'
  has_many :messages

  validates :guest_id,  presence: true
  validates :sitter_id, presence: true

  def unread_count(user_id)
    self.messages.where.not("(has_read = ?) OR (user_id = ?)", true, user_id).count
  end
end
