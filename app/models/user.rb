class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one_attached :icon

  has_many :cards, dependent: :destroy

  has_many :sitter_rooms, class_name: 'Room', foreign_key: 'sitter_id', dependent: :destroy

  has_many :guest_rooms,  class_name: 'Room', foreign_key: 'guest_id', dependent: :destroy

  has_many :sitter, through: :guest_rooms, source: 'guest'

  has_many :guest,  through: :sitter_rooms, source: 'sitter'

  has_many :messages

  before_create :set_default_icon

  def set_default_icon
    self.icon.attach(io:File.open(Rails.root.join('app', 'assets', 'images', 'default-icon.png')), filename: 'default-icon.png', content_type: 'image/png')
    # user.icon.attach(io: File.open('/app/assets/images'), filename: 'default.svg', content_type: 'icon/svg')
  end

  def unread_messages_count(user_id)
    count = 0
    self.sitter_rooms.each do |room|
      count += room.messages.where.not("(has_read = ?) OR (user_id = ?)", true, user_id).count
    end

    self.guest_rooms.each do |room|
      count += room.messages.where.not("(has_read = ?) OR (user_id = ?)", true, user_id).count
    end

    count
  end
end
