Rails.application.routes.draw do
  devise_for :users, controllers: {
      registrations: 'users/registrations'
  }

  root 'home#index'
  get  '/flash_close' => 'home#flash_close'
  get  '/contact'     => 'home#contact'
  get  '/about'       => 'home#about'

  scope :flower_sitter do
    get '/agreement'             => 'home#fs_agreement'
    post '/agreement_check'      => 'home#agreement_check'
    get '/welcome'               => 'home#fs_welcome'
    post '/first_create'         => 'home#first_create'
    get '/congrats/:id'          => 'home#fs_congrats'
  end

  resources :cards

  resources :users, except: :index

  resources :rooms

  resources :messages

  get '/portfolio' => 'home#portfolio'
end
