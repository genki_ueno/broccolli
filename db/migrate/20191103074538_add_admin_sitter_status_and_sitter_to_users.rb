class AddAdminSitterStatusAndSitterToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :admin, :boolean, default: false
    add_column :users, :name, :string
    add_column :users, :sitter_status, :integer, default: 0
    add_column :users, :sitter, :boolean, default: false
    add_column :users, :profile, :text, default: 'よろしくお願いします'
  end
end
