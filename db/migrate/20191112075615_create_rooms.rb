class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms do |t|
      t.integer :guest_id
      t.integer :sitter_id

      t.timestamps
    end

    add_index :rooms, :guest_id
    add_index :rooms, :sitter_id
    add_index :rooms, [:guest_id, :sitter_id], unique: true
  end
end
