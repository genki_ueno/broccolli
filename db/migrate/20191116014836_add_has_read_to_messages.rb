class AddHasReadToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :has_read, :boolean, default: false
  end
end
